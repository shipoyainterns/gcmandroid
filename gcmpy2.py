from pushjack import GCMClient

client = GCMClient(api_key='AIzaSyDkua9TXS1oLluF4GrjHMBNQZ0PX3dOsbc')

registration_id = 'fsIqmW32kb8:APA91bHZnJw9_zJTkuQ0ythgKpRSOHO40S2mywESYEiEw3nG40kG9Dk6ey0XWl_FzJ6zD8Fkid_6dZH6YMOYS-eXTifYDTxvkArr3OEFar-a8-n1EfUsi-IFR_sInvyZCaVMGFAnbHNT'
notification = {'title': 'Shipoya', 'body': 'Thanks for registering', 'icon': 'icon.png'}
alert = {'notification': notification}

# Send to single device.
# NOTE: Keyword arguments are optional.
res = client.send(registration_id,
                  alert,
                  notification=notification,
                  collapse_key='collapse_key',
                  delay_while_idle=True,
                  time_to_live=604800)

# Send to multiple devices by passing a list of ids.
client.send([registration_id], alert)