from pushjack import GCMClient
from flask import *

app=Flask("__name__")

@app.route('/',methods=['GET','POST'])
def fun():
	# if request.method=='POST':
	# 	title=request.form['title']
	# 	body=request.form['body']
	# 	return render_template('index.html')
	if request.method=='GET':
		title=request.args.get('title')
		body=request.args.get('body')
		if title==None and body==None:
			return render_template("index.html")
		client = GCMClient(api_key='AIzaSyDkua9TXS1oLluF4GrjHMBNQZ0PX3dOsbc')
		registration_ids = ['eojxt7tlY8Y:APA91bEG0hzIOzNI4cVQ799e_AG3daFKbn_qi2BqEygf0DiJs0pSE1PNMVHJPok9xBR6zZDTnfQUaOdkRMD9HauZi7DDb5wc44FnIza-xC8spIfnQpxdghzA8vbIt0M0qewNL8dgGkPP','fsIqmW32kb8:APA91bHZnJw9_zJTkuQ0ythgKpRSOHO40S2mywESYEiEw3nG40kG9Dk6ey0XWl_FzJ6zD8Fkid_6dZH6YMOYS-eXTifYDTxvkArr3OEFar-a8-n1EfUsi-IFR_sInvyZCaVMGFAnbHNT']
		notification = {'title': title, 'body': body}
		alert = {'notification': notification}

		#Send to single device.
		#NOTE: Keyword arguments are optional.
		# for registration_id in registration_ids:
		# 	res = client.send(registration_id,
		# 	                  alert,
		# 	                  notification=notification,
		# 	                  collapse_key='collapse_key',
		# 	                  delay_while_idle=True,
		# 	                  time_to_live=604800)
		# 	print(res)

		# Send to multiple devices by passing a list of ids.
		res=client.send(registration_ids, alert)
		responses=res.responses
		i=0
		print(res.responses)
		# dispmsgs=[]
		# for response in responses:
		# 	response2=str(response)
		# 	if response2=="<Response [401]>":
		# 		obj="Registration id - "+str(registration_ids[i])+"\n\nThere was an error authenticating the sender account - (Code 401)"
		# 	elif response2=="<Response [400]>":
		# 		obj="Registration id - "+str(registration_ids[i])+"\n\nThe request could not be parsed as JSON, or it contained invalid fields -(Code 400)"
		# 	elif response2=="<Response [200]>":
		# 		obj="Registration id - "+str(registration_ids[i])+"\n\nSuccessfully Sent the Notification - (Code 200)"
		# 	else:
		# 		obj="Registration id - "+str(registration_ids[i])+"\n\nAn Internal error in the GCM connection server while trying to process the request / Server is temporarily unavailable / Timeout Occured - (Code 5xx)"
		# 	dispmsgs.append(obj)
		# 	i+=1
		return render_template("thanks.html",failures=res.failures,responses=res.responses)
if __name__=='__main__':
	app.run(debug=True)